/*
Activity:
1. Create a GET route that will access the "/home" route that will print out a simple message.
2. Process a GET request at the "/home" route using postman.
3. Create a GET route that will access the "/users" route that will retrieve all the users in the mock database.
4. Process a GET request at the "/users" route using postman.
5. Create a DELETE route that will access the "/delete-user" route to remove a user from the mock database.
6. Process a DELETE request at the "/delete-user" route using postman.
7. Export the Postman collection and save it inside the root folder of our application.
8. Create a git repository named S34.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.

*/

const express = require("express");

const app = express();

const port = 3001;

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// ================================================================================

// GET /home
app.get("/home", (req, res) => {


	res.send("Welcome to the home page!");
});

// ================================================================================

// GET /user

let user = [];

app.get("/user", (req,res) => {
	
	console.log(req.body);

	if(req.body.username !==  '' && req.body.password !== ''){


		user.push(req.body);


		res.send(user);
	} else {


		res.send("Please input your username.")
	};
});



// =====================================================================
// DELETE


let deleteUser = [];

app.delete("/delete-user", (req,res) => {
	
	console.log(req.body);

	if(req.body.username !==  ''){


		deleteUser.pop(req.body);


		res.send(`User ${req.body.username} has been deleted.`);
	}
});
app.listen(port, () => console.log(`Server running at port ${port}`))







// /*
// // Keywords:


// // [Section] Activity
// // This route expects to receive a GET request at the URI "/home"
// app.get("/home", (req, res) => {
//     res.send("Welcome to the home page");
// })

// // This route expects to receive a GET request at the URI "/users"
// // This will retrieve all the users stored in the variable created above
// app.get("/users", (req, res) => {
//     res.send(users);
// })

// // This route expects to receive a DELETE request at the URI "/delete-user"
// // This will remove the user from the array for deletion
// app.delete("/delete-user", (req, res) => {

//     // Creates a variable to store the message to be sent back to the client/Postman 
//     let message;

//     /*
//         .pop(users)
//         aLEX - 0
//         BEn -1 
 
//     */

//     // Creates a condition if there are users found in the array
//     if (users.length != 0){

//         // Creates a for loop that will loop through the elements of the "users" array
//         for(let i = 0; i < users.length; i++){

//             // If the username provided in the client/Postman and the username of the current object in the loop is the same
//             if (req.body.username == users[i].username) {

//                 // The splice method manipulates the array and removes the user object from the "users" array based on it's index
//                 // users[i] is used here to indicate the start of the index number in the array for the element to be removed
//                 // The number 1 defines the number of elements to be removed from the array
//                 users.splice(users[i], 1);

//                 // Changes the message to be sent back by the response
//                 message = `User ${req.body.username} has been deleted.`;

//                 break;

//             } 

//         }

//     // If no user was found
//     } else {

//         // Changes the message to be sent back by the response
//         message = "User does not exist.";

//     }
    
//     // Sends a response back to the client/Postman once the user has been deleted or if a user is not found
//     res.send(message);

// })

// */